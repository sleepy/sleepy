# sleepy - sleep analysis
# Copyright (C) 2019  MRC Epidemiology Unit, University of Cambridge
#   
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
#   
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#   
# You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
from enum import Enum
from abc import ABC, abstractmethod
import numpy as np
import math
import statistics
from datetime import datetime, time, date, timedelta
import progressbar
from .utilities import *
from .scoring import *
from .rescoring import *
from .types import *
from .calculation import cole_kripke
from .calculation import tudor_locke
from .calculation import tudor_locke_rsa
from .calculation import count_scaled
from .calculation import sadeh
from .results import SleepResult, SleepDay
from copy import deepcopy
from pampro.pampro import Time_Series, Channel
import io, logging

class SleepAnalysis():

    def __init__(self, time_series, info,         
        scoring_method = SleepyScoringMethod.SADEH, 
        rescoring_method: SleepyRescoringMethod = SleepyRescoringMethod.SIA, 
        calculation_method: SleepyCalculationMethod = SleepyCalculationMethod.TUDOR_LOCKE):

        self.ts = time_series
        self.info = info
        self.scoring_method = scoring_method
        self.rescoring_method = rescoring_method
        self.calculation_method = calculation_method

        self.results = SleepResult()
        # The config provided at runtime
        self.config = None
        # The global config
        self.configGlobal = None
        # The calculation config
        self.configCalculation = None

        self.epoch_period = None
        self.activity_data = None
        self.inclinometer_data = None
        self.sleep_states = None
        self.sleep_timestamps = None
        self.sleep_timeframe = None

        self.add_analysis_channels()
        self.set_analysis_channel()

    def set_sleep_timestamps(self):
        self.sleep_timestamps = np.arange(self.sleep_timeframe[0].timestamp(), self.sleep_timeframe[1].timestamp(),
                                          self.epoch_period)
        while len(self.sleep_timestamps) < len(self.activity_data):
            # add an extra timestamp
            last_timestamp = self.sleep_timestamps[-1]
            self.sleep_timestamps = np.append(self.sleep_timestamps, last_timestamp + self.epoch_period)

    def add_analysis_channels(self):
        channel_names = self.ts.get_channel_names()

        if 'inclinometer' in channel_names:
            self.inclinometer_channel = self.ts.get_channel('inclinometer')
        elif 'lying' in channel_names:
            lying_channel = self.ts.get_channel('lying')
            sitting_channel = self.ts.get_channel('sitting')
            standing_channel = self.ts.get_channel('standing')
            inclinometer_channel = lying_channel.clone_as('inclinometer')
            for i in range(len(inclinometer_channel.data)):
                if standing_channel.data[i] > 0:
                    inclinometer_channel.data[i] = 3
                elif sitting_channel.data[i] > 0:
                    inclinometer_channel.data[i] = 2
                elif lying_channel.data[i] > 0:
                    inclinometer_channel.data[i] = 1
                else:
                    inclinometer_channel.data[i] = 0
            self.inclinometer_data = inclinometer_channel.data
        
        if 'vector_magnitude' not in channel_names:                
            if 'counts_x' in channel_names and 'counts_y' in channel_names and 'counts_z' in channel_names:
                counts_x_channel = self.ts.get_channel('counts_x')
                counts_y_channel = self.ts.get_channel('counts_y')
                counts_z_channel = self.ts.get_channel('counts_z')
                activity_channel = counts_x_channel.clone_as('vector_magnitude')
                for i in range(len(activity_channel.data)):
                    activity_channel.data[i] = math.sqrt(
                        math.pow(counts_x_channel.data[i], 2) + math.pow(counts_y_channel.data[i], 2) + math.pow(
                            counts_z_channel.data[i], 2))
                self.ts.add_channel(activity_channel)

    def set_analysis_channel(self):
        channel_names = self.ts.get_channel_names()
            
        # SAdeh uses Counts X
        if self.scoring_method == SleepyScoringMethod.SADEH:
            if 'counts_x' in channel_names:
                activity_channel = self.ts.get_channel('counts_x')
            elif 'counts' in channel_names:
                activity_channel = self.ts.get_channel('counts')   
            elif 'vector_magnitude' in channel_names:
                activity_channel = self.ts.get_channel('vector_magnitude')                
            else:
                raise RuntimeError("Could not find a suitable counts channel")
            
        # Count scaled uses vector magnitude if possible
        elif self.scoring_method == SleepyScoringMethod.COUNT_SCALED:
            if 'vector_magnitude' in channel_names:
                activity_channel = self.ts.get_channel('vector_magnitude')
            elif 'counts' in channel_names:
                activity_channel = self.ts.get_channel('counts')    
            elif 'counts_x' in channel_names:
                activity_channel = self.ts.get_channel('counts_x')                
            else:
                raise RuntimeError("Could not find a suitable counts channel")

        elif self.scoring_method == SleepyScoringMethod.COLE_KRIPKE:
            if 'counts_x' in channel_names:
                activity_channel = self.ts.get_channel('counts_x')
            elif 'counts' in channel_names:
                activity_channel = self.ts.get_channel('counts')   
            elif 'vector_magnitude' in channel_names:
                activity_channel = self.ts.get_channel('vector_magnitude')                
            else:
                raise RuntimeError("Could not find a suitable counts channel")

        self.epoch_period = 1 / activity_channel.frequency
        self.activity_data = activity_channel.data        
        self.generateTimestamps(activity_channel)

    def generateTimestamps(self, activity_channel):
        if not activity_channel.timestamp_policy == 'offset':
            raise SleepyException("Timestamp policy not offset")
        
        firstTimestamp = activity_channel.start.timestamp()
        lastTimestamp = firstTimestamp + np.max(activity_channel.timestamps)/1000
        finish = datetime.fromtimestamp(lastTimestamp, tz=activity_channel.start.tzinfo)
        self.sleep_timestamps = np.arange(firstTimestamp, lastTimestamp, self.epoch_period)

        if len(self.sleep_timestamps) > len(self.activity_data):
            raise SleepyException("Timestamp epochs exceed activity epochs")
        elif len(self.activity_data) - len(self.sleep_timestamps) > 10:
            raise SleepyException("Timestamp and activity epochs mismatch")
        else:
            while len(self.sleep_timestamps) < len(self.activity_data):
                # add an extra timestamp
                last_timestamp = self.sleep_timestamps[-1]
                self.sleep_timestamps = np.append(self.sleep_timestamps, last_timestamp + self.epoch_period)
        
        start = datetime.fromtimestamp(self.sleep_timestamps[0], tz=activity_channel.start.tzinfo)
        finish = datetime.fromtimestamp(self.sleep_timestamps[-1], tz=activity_channel.start.tzinfo)
        self.sleep_timeframe = (start, finish)

    def set_config(self, config):

        # Convert any time_in_bed, time_out_bed into "time" classes
        if ('time_in_bed' in config) and isinstance(config['time_in_bed'], str):
            if len(config['time_in_bed']) < 7:
                config['time_in_bed'] = datetime.strptime(config['time_in_bed'],'%H:%M').time()
            else:
                config['time_in_bed'] = datetime.strptime(config['time_in_bed'],'%H:%M:%S').time()

        if ('time_out_bed' in config) and isinstance(config['time_out_bed'], str):
            if len(config['time_out_bed']) < 7:
                config['time_out_bed'] = datetime.strptime(config['time_out_bed'],'%H:%M').time()
            else:
                config['time_out_bed'] = datetime.strptime(config['time_out_bed'],'%H:%M:%S').time()
    
        default_config = {
            'noon_to_noon': True,
            'time_in_bed': time(19,30,0,0),
            'time_out_bed': time(6,0,0,0),
            'epoch_period_scoring': None,
            'epoch_period_calculation': 60,
            'wakening-mins': 5,
            'nap-mins': 5,
            'pre-hours': 6,
            'post-hours': 6
        }

        # First at the overall default config
        self.config = set_config(config, default_config)
        
    def analyse(self, customConfig = None):
        self.set_config(customConfig)        
        
        """
        Split the complete file into days before analysing
        """
        self.split_days(self.config['pre-hours'], self.config['post-hours'])

        """
        Process the file day-by-day
        """
        for day in self.results.days:
            """
            If the scoring epoch period is set then resample
            Default is not to resample
            """
            if self.config['epoch_period_scoring'] is not None:
                day = self.resample(day, self.config['epoch_period_scoring'])

            """
            Determine the epoch SLEEP_STATES
            """
            day = self.score(day)
            
            """
            Rescore the epoch SLEEP_STATES
            """
            day = self.rescore(day)

            """
            Resample
            """
            day = self.resample(day, self.config['epoch_period_calculation'])
            
            """
            Add date
            """
            day.datetime = datetime.fromtimestamp((day.timeframe[0].timestamp()+day.timeframe[1].timestamp())/2, tz=day.timezone)

            try:
                day = self.calculate(day, self.config)
            except SleepyException as e:
                logging.info("{0}. Day was not valid: {1}".format(self.info["generic_filename"], e))
                print("\tDay was not valid: {0}".format(e))
            except Exception as e:
                logging.warning("{0}. Day was not valid: {1}".format(self.info["generic_filename"], e))
                print("\tDay was not valid: {0}".format(e))
            
                   
    def score(self, day):
        if self.scoring_method == SleepyScoringMethod.SADEH:
            day.sleep_states = score_sadeh(day.activity_data, epoch_period=day.epoch_period)

        elif self.scoring_method == SleepyScoringMethod.COLE_KRIPKE:
            day.sleep_states = score_cole_kripke(day.activity_data, epoch_period=day.epoch_period)

        elif self.scoring_method == SleepyScoringMethod.COUNT_SCALED:
            day.sleep_states = score_count_scaled(day.activity_data, epoch_period=day.epoch_period)

        else:
            raise Exception('Not implemented')
        return day

    def rescore(self, day):
        if self.rescoring_method == SleepyRescoringMethod.WEBSTER:
            day.sleep_states = rescore_webster(day.sleep_states, day.epoch_period)
        
        elif self.rescoring_method == SleepyRescoringMethod.COUNT_SCALED:
            day.sleep_states = rescore_count_scaled(day.sleep_states)

        elif self.rescoring_method == SleepyRescoringMethod.SIA and day.inclinometer_data is not None:
            day.sleep_states = rescore_inclinometer(day.sleep_states, day.inclinometer_data)
        
        return day

    def resample(self, day, new_epoch_period):
        if new_epoch_period == day.epoch_period:
            return day

        day.activity_data = resample_counts(day.activity_data, day.epoch_period, new_epoch_period)        
        if not day.inclinometer_data is None:
            day.inclinometer_data = resample_inclinometer(day.inclinometer_data, day.epoch_period, new_epoch_period)

        if not day.sleep_states is None:
            day.sleep_states = resample_sleep_states(day.sleep_states, day.epoch_period, new_epoch_period)

        day.update_timestamps(new_epoch_period)
        day.epoch_period = new_epoch_period

        return day

    def calculate(self, day, config):
        """
        Calculate the results
        """
        if self.calculation_method == SleepyCalculationMethod.TUDOR_LOCKE:
            day = tudor_locke.calculate_day(day, config)
        
        elif self.calculation_method == SleepyCalculationMethod.TUDOR_LOCKE_RSA:
            day = tudor_locke_rsa.calculate_day(day, config)
        
        elif self.calculation_method == SleepyCalculationMethod.LOG_ACC:
            raise Exception("Not implemented")
            #self.results = log_acc._calculate_day(day, config)

        elif self.calculation_method == SleepyCalculationMethod.COLE_KRIPKE:
            day = cole_kripke.calculate_day(day, config)

        elif self.calculation_method == SleepyCalculationMethod.COUNT_SCALED:
            day = count_scaled.calculate_day(day, config)

        elif self.calculation_method == SleepyCalculationMethod.SADEH:
            day = sadeh.calculate_day(day, config)

        else:
            raise Exception('Not implemented') 
        
        return day       

    def split_days(self, pre_hours = 0, post_hours = 0):       
        
        start_time = self.sleep_timeframe[0]
        start_time = start_time.replace(hour=12, minute=0, second=0)
            
        start_datetime = start_time - timedelta(hours=pre_hours)
        end_datetime = start_time + timedelta(days=1)+ timedelta(hours=post_hours)

        self.results.days = []

        splitting = True
        while splitting:
            start_timestamp = start_datetime.timestamp()
            end_timestamp = end_datetime.timestamp()

            inds = (self.sleep_timestamps >= start_timestamp) & (self.sleep_timestamps < end_timestamp)

            if (sum(inds) > 121):
                sleep_day = SleepDay()
                sleep_day.timezone = start_datetime.tzinfo
                sleep_day.epoch_period = self.epoch_period
                sleep_day.noon_to_noon = True
                sleep_day.timestamps = self.sleep_timestamps[inds]
                sleep_day.activity_data = self.activity_data[inds]
                if not self.inclinometer_data is None:
                    sleep_day.inclinometer_data = self.inclinometer_data[inds]
                #sleep_day.sleep_states = self.sleep_states[inds]
                sleep_day.timeframe = [start_datetime, end_datetime]
                sleep_day.update_timeframe()                
                time_in_bed = self.config['time_in_bed'] 
                time_out_bed = self.config['time_out_bed'] 
                sleep_day.time_in_bed = datetime(sleep_day.date.year,sleep_day.date.month,sleep_day.date.day,time_in_bed.hour,time_in_bed.minute,time_in_bed.second, tzinfo=sleep_day.timezone)
                sleep_day.time_out_bed = datetime(sleep_day.date.year,sleep_day.date.month,sleep_day.date.day,time_out_bed.hour,time_out_bed.minute,time_out_bed.second, tzinfo=sleep_day.timezone)+timedelta(days=1)
                self.results.days.append(sleep_day)
            else:
                splitting = False

            start_datetime = start_datetime + timedelta(days=1)
            end_datetime = end_datetime + timedelta(days=1)  
    
    def plot_sleep(self, save = True):       

        #TODO - Setup displays for Noon to Noon analysis also

        for day in self.results.days:
            if day.valid:
                if save:
                    plot_day_sleep(day, day.date.isoformat())
            
